#include <stdio.h>
#include <neopop.h>
#include <allegro.h>

#include "Neopop_conf.h"

extern int run_menu;
int neopop_over = 0;
_u8 system_frameskip_key;
char loaded_rom_fn[256];

int load_rom(char* file)
{
	FILE* fp = fopen(file, "rb");
	if(fp == NULL) {
		printf("Can't open %s\n", file);
		return 0;
	}
	rom_unload();
	fseek(fp, 0, SEEK_END);
	rom.length = ftell(fp);
	rom.data = calloc(rom.length, 1);
	fseek(fp, 0, SEEK_SET);
	fread(rom.data, rom.length, 1, fp);
	fclose(fp);
	rom_loaded();
	strncpy(loaded_rom_fn, file, 256);
	return 1;
}

void system_message(char *vaMessage, ...)
{
	char message[1000];
	va_list vl;

	va_start(vl, vaMessage);
	vsprintf(message, vaMessage, vl);
	va_end(vl);
	
	printf("[M] %s\n", message);
}

int main(int argc, char *argv[])
{
	int i;
	printf("%s %s\n", PROGRAM_NAME, NEOPOP_VERSION);
	printf("Emulator as in Dreamland\n");
	printf("Mac port by:\n" \
		"[*] trap15\n\n");
	config_load();
	if (allegro_init() != 0)
		return 1;
	nposx_video_setup();
	install_keyboard();
	install_mouse();
	install_timer();
	enable_hardware_cursor();
	if(!bios_install()) {
		printf("Cannot install bios :(\n");
	}
	
	language_english = TRUE; // Set to english
	system_frameskip_key = 1;
	system_colour = COLOURMODE_AUTO;
	mute = FALSE;
	
	char *file;
	if(argc < 2) {
		file = dialog_rom_file();
		if(!file) {
			printf("No file selected.\n");
			return 0;
		}
	}else
		file = argv[1];
	if(!load_rom(file)) {
		goto neopop_done;
	}
	
	reset();
	if(!system_sound_init()) {
		printf("Sound init failed.\n");
		goto neopop_done;
	}
	while(!neopop_over) {
#ifdef NEOPOP_DEBUG
		emulate_debug(1, 1);
#else
		emulate();
#endif
		if(run_menu) {
			show_menu();
			run_menu = 0;
		}
	}
neopop_done:
	nposx_video_kill();
	return 0;
}

END_OF_MAIN()
