#include <neopop.h>

void system_comms_write(unsigned char data)
{
	return;
}

BOOL system_comms_read(unsigned char *buffer)
{
	return FALSE;
}

BOOL system_comms_poll(unsigned char *buffer)
{
	return system_comms_read(buffer);
}

void system_debug_history_clear() 
{
	return;
}
void system_debug_history_add() 
{
	return;
}
void system_debug_stop() 
{
	return;
}
void system_debug_message_associate_address(_u32 addr) 
{
	return;
}
void system_debug_refresh() 
{
	return;
}
void system_debug_clear() 
{
	return;
}

void system_debug_message(char* vaMessage,...)
{
	va_list vl;
	va_start(vl, vaMessage);
	vfprintf(stderr, vaMessage, vl);
	va_end(vl);
}

