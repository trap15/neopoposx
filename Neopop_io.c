#include <stdio.h>
#include <neopop.h>

extern char loaded_rom_fn[];

int system_io_read_file(char* filename, _u8* buffer, _u32 bufferLength)
{
	FILE * fp = NULL;
	fp = fopen(filename, "rb");
	if(fp == NULL) {
		return FALSE;
	}
	if(fread(buffer, 1, bufferLength, fp) != bufferLength) {
		fclose(fp);
		return FALSE;
	}
	fclose(fp);
	return TRUE;
}

int system_io_write_file(char* filename, _u8* buffer, _u32 bufferLength)
{
	FILE * fp = NULL;
	fp = fopen(filename, "rb");
	if(fp == NULL) {
		return FALSE;
	}
	if(fwrite(buffer, 1, bufferLength, fp) != bufferLength) {
		fclose(fp);
		return FALSE;
	}
	fclose(fp);
	return TRUE;
}

BOOL system_io_rom_read(char* filename, _u8* buffer, _u32 bufferLength)
{
	printf("%s(%s, %016llX, %d)\n", __FUNCTION__, filename, buffer, bufferLength);
	return TRUE;
}

BOOL system_io_flash_read(_u8* buffer, _u32 bufferLength)
{
	BOOL ret;
	char *outfile;
	printf("%s\n", __FUNCTION__);
	outfile = malloc(strlen(loaded_rom_fn) + 5);
	sprintf(outfile, "%s.ngf", loaded_rom_fn);
	ret = system_io_read_file(outfile, buffer, bufferLength);
	free(outfile);
	return ret;
}

BOOL system_io_flash_write(_u8* buffer, _u32 bufferLength)
{
	BOOL ret;
	char *outfile;
	printf("%s\n", __FUNCTION__);
	outfile = malloc(strlen(loaded_rom_fn) + 5);
	sprintf(outfile, "%s.ngf", loaded_rom_fn);
	ret = system_io_write_file(outfile, buffer, bufferLength);
	free(outfile);
	return ret;
}

BOOL system_io_state_read(char* filename, _u8* buffer, _u32 bufferLength)
{
	return system_io_read_file(filename, buffer, bufferLength);
}

BOOL system_io_state_write(char* filename, _u8* buffer, _u32 bufferLength)
{
	return system_io_write_file(filename, buffer, bufferLength);
}
