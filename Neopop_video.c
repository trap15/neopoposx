#include <allegro.h>

#include "neopop.h"
#include "gfx.h"

#include "Neopop_conf.h"

#define ZOOM_LEVEL		(npconf.scale)
#define XRGB4_TO_RGBA8(x)	(((((x >> 8) & 0xF) * 255 / 15) <<  0) | \
				 ((((x >> 4) & 0xF) * 255 / 15) <<  8) | \
				 ((((x >> 0) & 0xF) * 255 / 15) << 16))
#define FPS_DISPLAY_X		(0)
#define FPS_DISPLAY_Y		(0)

extern int neopop_over;
extern FILE* ngpmfile;

int log_ngpm = 0;
int run_menu = 0;
int current_page;
BITMAP *video_page[2];
int counter = 0;

volatile int update_count = 0;
volatile int frame_count = 0;
volatile int fps = 0;
volatile int keystates[__allegro_KEY_MAX];

#define KEYDEF_SIZE	(11)

static int keydeflist[KEYDEF_SIZE] = {
	__allegro_KEY_UP,	/* Up */
	__allegro_KEY_DOWN,	/* Down */
	__allegro_KEY_LEFT,	/* Left */
	__allegro_KEY_RIGHT,	/* Right */
	__allegro_KEY_Z,	/* A */
	__allegro_KEY_X,	/* B */
	__allegro_KEY_C,	/* Start */
	__allegro_KEY_ESC,	/* Quit */
	__allegro_KEY_F,	/* FPS Show */
	__allegro_KEY_M,	/* Menu */
	__allegro_KEY_L,	/* Log NGPM */
};

static void timer_proc(void)
{
	update_count++;
}

END_OF_FUNCTION(timer_proc)

static void fps_proc(void)
{
	fps = frame_count;
	frame_count = 0;
}

END_OF_FUNCTION(fps_proc)

void keypress_handler(int scancode)
{
	_u32 keys = 0;
	int x;
	keystates[scancode & 0x7F] = (scancode & 0x80) ? 0 : 1;
	for(x = 0; x < 7; x++) {
		if(keystates[keydeflist[x]]) {
			keys |= (1 << x);
		}
	}
	ram[0x6F82] = keys | (1 << 7);
}
END_OF_FUNCTION(keypress_handler)

void system_VBL(void)
{
	static int show_fps = 0;
	static int bpressed[KEYDEF_SIZE] = { 0, };
	_u32* outline;
	int x, y;
#if LOG_NGPM
	char ident[4] = {'N', 'G', 'P', 'M'};
	uint32_t head_size = htonl(0x20);
	uint32_t fsz = htonl(0);
	uint32_t version = htonl((1 << 16));
	uint32_t ngpi_off = htonl(0);
	uint32_t music_clock = htonl(SOUNDCHIPCLOCK);
	uint32_t reserve = htonl(0);
#endif
	for(x = 7; x < KEYDEF_SIZE; x++) {
		if(keystates[keydeflist[x]]) {
			if(!bpressed[x]) {
				switch(x - 7) {
					case 0: /* Quit */
						neopop_over = 1;
						break;
					case 1:	/* FPS Show */
						show_fps ^= 1;
						break;
					case 2:	/* Menu */
						run_menu = 1;
						break;
#if LOG_NGPM
					case 3:	/* Log NGPM */
						log_ngpm ^= 1;
						if(log_ngpm) {
							ngpmfile = fopen("logged.ngpm", "wb+");
							fwrite(ident, 4, 1, ngpmfile);
							fwrite(&head_size, 4, 1, ngpmfile);
							fwrite(&fsz, 4, 1, ngpmfile);
							fwrite(&version, 4, 1, ngpmfile);
							fwrite(&ngpi_off, 4, 1, ngpmfile);
							fwrite(&music_clock, 4, 1, ngpmfile);
							fwrite(&reserve, 4, 1, ngpmfile);
							fwrite(&reserve, 4, 1, ngpmfile);
						}else{
							fsz = ftell(ngpmfile);
							fsz = htonl(fsz);
							fseek(ngpmfile, 0x08, SEEK_SET);
							fwrite(&fsz, 4, 1, ngpmfile);
							fclose(ngpmfile);
						}
						break;
#endif
				}
				bpressed[x] = 1;
			}
		}else{
			bpressed[x] = 0;
		}
	}
	if(!frameskip_count) {
		while(update_count < 1) {
			rest(1);
		}
		update_count = 0;
		frame_count++;
		for(y = 0; y < SCREEN_H; y++) {
			outline = (_u32*)(video_page[current_page]->line[y]);
			for(x = 0; x < SCREEN_W; x++) {
				outline[x] = XRGB4_TO_RGBA8(cfb[((y / ZOOM_LEVEL) * SCREEN_WIDTH) + (x / ZOOM_LEVEL)]);
			}
		}
		if(show_fps)
			textprintf_ex(video_page[current_page], font, FPS_DISPLAY_X, FPS_DISPLAY_Y, makecol(0, 0, 0), makecol(255, 255, 255), "%d", fps);
		show_video_bitmap(video_page[current_page]);
		current_page ^= 1;
	}
	system_sound_update();
}

void nposx_video_reload()
{
	current_page = 0;
	set_color_depth(32);
	if(set_gfx_mode(GFX_AUTODETECT, SCREEN_WIDTH * ZOOM_LEVEL, SCREEN_HEIGHT * ZOOM_LEVEL, 0, 0) != 0) {
		printf("Couldn't set screen res.\n");
	}
	video_page[0] = create_video_bitmap(SCREEN_W, SCREEN_H);
	video_page[1] = create_video_bitmap(SCREEN_W, SCREEN_H);
	if((video_page[0] == NULL) || (video_page[1] == NULL)) {
		printf("Couldn't create video bitmaps.\n");
	}	
}

void nposx_video_setup()
{
	int i;
	nposx_video_reload();
	for(i = 0; i < __allegro_KEY_MAX; i++)
		keystates[i] = 0;
	
	update_count = 0;
	frame_count = 0;
	fps = 0;
	LOCK_VARIABLE(update_count);
	LOCK_VARIABLE(frame_count);
	LOCK_VARIABLE(fps);
	LOCK_VARIABLE(keystates);
	LOCK_FUNCTION(timer_proc);
	LOCK_FUNCTION(fps_proc);
	LOCK_FUNCTION(keypress_handler);
	keyboard_lowlevel_callback = keypress_handler;
	
	install_int_ex(timer_proc, BPS_TO_TIMER(60));
	install_int_ex(fps_proc, BPS_TO_TIMER(1));
	
	/* Without this, button holds would be fucked */
	set_keyboard_rate(0, 0);
}

void nposx_video_kill()
{
#if LOG_NGPM
	uint32_t fsz;
	if(log_ngpm) {
		fsz = ftell(ngpmfile);
		fsz = htonl(fsz);
		fseek(ngpmfile, 0x08, SEEK_SET);
		fwrite(&fsz, 4, 1, ngpmfile);
		fclose(ngpmfile);
		log_ngpm = 0;
	}
#endif	
	destroy_bitmap(video_page[0]);
	destroy_bitmap(video_page[1]);
}
