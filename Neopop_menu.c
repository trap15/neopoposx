#include <stdio.h>
#include <neopop.h>

#include <allegro.h>

#include "Neopop_conf.h"

extern int neopop_over;
extern int current_page;
extern BITMAP *video_page[2];
extern volatile int update_count;
extern volatile int frame_count;
static BOOL oldmute;
extern neopop_config_t npconf;

static char path[256] = {0,};
#if !USING_OSX
void set_base_path(char* path)
{
	
}
#endif

char *dialog_rom_file()
{
	int ret = 0;
	if(path[0] == 0) {
		set_base_path(path);
	}
	ret = file_select_ex("Choose a ROM", path, "NGP;NGC;NPC", 256, 0, 0);
	if(!ret)
		return NULL;
	return path;
}

int quit(void)
{
	if(alert("Really Quit?", NULL, NULL, "&Yes", "&No", 'y', 'n') == 1) {
		neopop_over = 1;
		return D_CLOSE;
	}else
		return D_O_K;
}

int about(void)
{
	alert("* Neopop Allegro *",
	      "",
	      "Emulator as in Dreamland",
	      "Ok", 0, 0, 0); 
	return D_O_K;
}

int menu_mute()
{
	oldmute ^= 1;
	if(oldmute)
		active_menu->flags |= D_SELECTED;
	else
		active_menu->flags &= ~D_SELECTED;
	return D_O_K;
}

int menu_loadrom()
{
	char *file = dialog_rom_file();
	if(file)
		if(load_rom(file))
			reset();
	return D_CLOSE;
}

int menu_reset()
{
	reset();
	return D_CLOSE;
}

#define FS_FRAME	0
#define FS_MESSAGE	1
#define FS_OK		2
#define FS_CANCEL	3

int apply_video();

int my_button_procx(int msg, DIALOG *d, int c)
{
	int ret = d_button_proc(msg, d, c);
	if(ret == D_CLOSE && d->dp3)
		return ((int (*)(void))d->dp3)();
	return ret;
}

int my_slider_proc(int msg, DIALOG *d, int c)
{
	int width = 0.95 * SCREEN_W + 1;
	int wpad = 0.05 * width + 1;
	int font_w;
	char tmp[64];
	DIALOG textdlg;
	DIALOG valdlg;
	DIALOG slidlg;
	memcpy(&textdlg, d, sizeof(DIALOG));
	textdlg.dp = strdup(textdlg.dp3);
	font_w = text_length(font, uconvert_ascii(textdlg.dp, tmp));
	textdlg.w = font_w;
	textdlg.x = wpad;
	d_text_proc(msg, &textdlg, c);
	free(textdlg.dp);
	memcpy(&valdlg, d, sizeof(DIALOG));
	valdlg.dp = malloc(10);
	sprintf(valdlg.dp, "%d ", d->d2 + 1);
	valdlg.x = textdlg.x + font_w + 4;
	valdlg.dp2 = NULL;
	font_w = text_length(font, uconvert_ascii("  ", tmp));
	valdlg.w = font_w;
	d_text_proc(msg, &valdlg, c);
	free(valdlg.dp);
	memcpy(&slidlg, d, sizeof(DIALOG));
	slidlg.x = valdlg.x + font_w + 4;
	slidlg.w = width - wpad * 3 - valdlg.w - font_w - 28;
	slidlg.dp  = NULL;
	slidlg.d2  = d->d2;
	d_slider_proc(msg, &slidlg, c);
	d->d2 = slidlg.d2;
	return D_O_K;
}

DIALOG video_config_dialog[] = {
	{ d_shadow_box_proc, 0, 0, 0, 0, 0, 0, 0,      0, 0, 0, NULL,           NULL, NULL },
	{ d_ctext_proc,      0, 0, 0, 0, 0, 0, 0,      0, 0, 0, "Video Config", NULL, NULL },
	{ my_button_procx,   0, 0, 0, 0, 0, 0, 0, D_EXIT, 0, 0, NULL,           NULL, apply_video },
	{ d_button_proc,     0, 0, 0, 0, 0, 0, 0, D_EXIT, 0, 0, NULL,           NULL, NULL },
	{ my_slider_proc,    0, 0, 0, 0, 0, 0, 0,      0, 5, 0, "Scale: ",      NULL, "Scale: " },
	{ d_yield_proc,      0, 0, 0, 0, 0, 0, 0,      0, 0, 0, NULL,           NULL, NULL },
	{ NULL,              0, 0, 0, 0, 0, 0, 0,      0, 0, 0, NULL,           NULL, NULL }
};

DIALOG audio_config_dialog[] = {
	{ d_shadow_box_proc, 0, 0, 0, 0, 0, 0, 0,      0, 0, 0, NULL,           NULL, NULL },
	{ d_ctext_proc,      0, 0, 0, 0, 0, 0, 0,      0, 0, 0, "Audio Config", NULL, NULL },
	{ my_button_procx,   0, 0, 0, 0, 0, 0, 0, D_EXIT, 0, 0, NULL,           NULL, apply_video },
	{ d_button_proc,     0, 0, 0, 0, 0, 0, 0, D_EXIT, 0, 0, NULL,           NULL, NULL },
	{ d_yield_proc,      0, 0, 0, 0, 0, 0, 0,      0, 0, 0, NULL,           NULL, NULL },
	{ NULL,              0, 0, 0, 0, 0, 0, 0,      0, 0, 0, NULL,           NULL, NULL }
};

int apply_video()
{
	npconf.scale = video_config_dialog[4].d2 + 1;
	nposx_video_kill();
	nposx_video_reload();
	broadcast_dialog_message(MSG_DRAW, 0);
	return D_CLOSE;
}

static void stretch_dialog(DIALOG *d, int width, int height)
{
	int font_w, font_h, hpad, vpad;
	char tmp[16];
	
	/* horizontal settings */
	font_w = text_length(font, uconvert_ascii("A", tmp));
	
	if (width == 0)
		width = 0.95 * SCREEN_W + 1;
	
	hpad = 0.05 * width + 1;
	
	d[FS_FRAME].w   = width;
	d[FS_FRAME].x   = 0;
	d[FS_MESSAGE].w = width - 2;
	d[FS_MESSAGE].x = 1;
	d[FS_OK].w      = 10*font_w + 1;
	d[FS_OK].x      = (d[FS_FRAME].w - 2*d[FS_OK].w - hpad + 1)/2; 
	d[FS_CANCEL].w  = d[FS_OK].w;
	d[FS_CANCEL].x  = d[FS_FRAME].w - d[FS_OK].x - d[FS_CANCEL].w;
	
	/* vertical settings */     
	font_h = text_height(font);
	
	if (height == 0)
		height = 0.95*SCREEN_H + 1;
	
	vpad = 0.04*height + 1;
	
	d[FS_FRAME].h   = height;
	d[FS_FRAME].y   = 0;
	d[FS_MESSAGE].h = font_h;
	d[FS_MESSAGE].y = vpad;
	d[FS_OK].h      = font_h + 9;
	d[FS_OK].y      = d[FS_FRAME].h - 1.5*vpad - d[FS_OK].h;
	d[FS_CANCEL].h  = d[FS_OK].h;
	d[FS_CANCEL].y  = d[FS_OK].y;
}

int audio_config()
{
	audio_config_dialog[FS_OK].dp = (void*)get_config_text("OK");
	audio_config_dialog[FS_CANCEL].dp = (void*)get_config_text("Cancel");
	stretch_dialog(audio_config_dialog, 0, 0);
	centre_dialog(audio_config_dialog);
	set_dialog_color(audio_config_dialog, gui_fg_color, gui_bg_color);
	return popup_dialog(audio_config_dialog, -1);
}

int video_config()
{
	int ret;
	int width = 0.95 * SCREEN_W + 1;
	int wpad = 0.05 * width + 1;
	int height = 0.95 * SCREEN_H + 1;
	int hpad = 0.04 * height + 1;	
	video_config_dialog[FS_OK].dp = (void*)get_config_text("OK");
	video_config_dialog[FS_CANCEL].dp = (void*)get_config_text("Cancel");
	video_config_dialog[4].w = width - wpad * 2;
	video_config_dialog[4].h = 12;
	video_config_dialog[4].x = wpad;
	video_config_dialog[4].y = hpad * 2;
	video_config_dialog[4].d2 = npconf.scale - 1;
	stretch_dialog(video_config_dialog, 0, 0);
	centre_dialog(video_config_dialog);
	set_dialog_color(video_config_dialog, gui_fg_color, gui_bg_color);
	ret = popup_dialog(video_config_dialog, -1);
	broadcast_dialog_message(MSG_DRAW, 0);
	return ret;
}

MENU gamemenu[] =
{
	{ "&Load ROM", menu_loadrom,  NULL,      0,  NULL  },
	{ "&Reset",      menu_reset,  NULL,      0,  NULL  },
	{ "&Quit",             quit,  NULL,      0,  NULL  },
	{ NULL,                NULL,  NULL,      0,  NULL  }
};

MENU audiomenu[] =
{
	{ "&Mute",         menu_mute,  NULL,      0,  NULL  },
	{ "&Configure", audio_config,  NULL,      0,  NULL  },
	{ NULL,                 NULL,  NULL,      0,  NULL  }
};

MENU videomenu[] =
{
	{ "&Configure", video_config,  NULL,      0,  NULL  },
	{ NULL,                 NULL,  NULL,      0,  NULL  }
};

MENU helpmenu[] =
{
	{ "&About",          about,  NULL,      0,  NULL  },
	{ NULL,               NULL,  NULL,      0,  NULL  }
};


MENU the_menu[] =
{
	{ "&Game",   NULL,   gamemenu,       0,      NULL  },
	{ "&Audio",  NULL,   audiomenu,      0,      NULL  },
	{ "&Video",  NULL,   videomenu,      0,      NULL  },
	{ "&Help",   NULL,   helpmenu,       0,      NULL  },
	{ NULL,      NULL,   NULL,           0,      NULL  }
};

int my_button_proc(int msg, DIALOG *d, int c)
{
	int ret = d_button_proc(msg, d, c);
	BITMAP *b;
	b = (BITMAP *)d->dp2;
	if(msg == MSG_DRAW)
		blit(b, gui_get_screen(), 0, 0, d->x, d->y, d->w, d->h);
	return ret;
}

DIALOG npdialog[] = {
	/* (dialog proc)     (x)   (y)   (w)   (h) (fg)(bg) (key) (flags)     (d1) (d2)    (dp)                   (dp2) (dp3) */
	{ my_button_proc,      0,   0,    0,    0,   0,  0,    0, D_EXIT,       0,   0,    "",                     NULL, NULL  },
	{ d_menu_proc,         0,   0,    0,    0,   0,  0,    0,      0,       0,   0,    the_menu,               NULL, NULL  },
	{ d_keyboard_proc,     0,   0,    0,    0,   0,  0,    0,      0,  KEY_F1,   0,    (void *)about,          NULL, NULL  },
	{ d_yield_proc,        0,   0,    0,    0,   0,  0,    0,      0,       0,   0,    NULL,                   NULL, NULL  },
	{ NULL,                0,   0,    0,    0,   0,  0,    0,      0,       0,   0,    NULL,                   NULL, NULL  }
};

void show_menu()
{
	int x, y;
	_u32 r, g, b, a;
	BITMAP* bgbmp;
	oldmute = mute;
	mute = TRUE;
	if(oldmute)
		audiomenu[0].flags |= D_SELECTED;
	else
		audiomenu[0].flags &= ~D_SELECTED;
	npdialog[0].w = SCREEN_W;
	npdialog[0].h = SCREEN_H;
	bgbmp = create_video_bitmap(SCREEN_W, SCREEN_H);
	blit(video_page[current_page], bgbmp, 0, 0, 0, 0, SCREEN_W, SCREEN_H);
	acquire_bitmap(bgbmp);
	for(x = 0; x < SCREEN_W; x++) {
		for(y = 0; y < SCREEN_H; y++) {
			r = (((_u32*)bgbmp->line[y])[x] >>  0) & 0xFF;
			g = (((_u32*)bgbmp->line[y])[x] >>  8) & 0xFF;
			b = (((_u32*)bgbmp->line[y])[x] >> 16) & 0xFF;
			a = (((_u32*)bgbmp->line[y])[x] >> 24) & 0xFF;
			r /= 2; g /= 2; b /= 2; a /= 2;
			((_u32*)bgbmp->line[y])[x] = (r << 0) | (g << 8) | (b << 16) | (a << 24);
		}
	}
	release_bitmap(bgbmp);
	npdialog[0].dp2 = bgbmp;
	
	DIALOG_PLAYER *npmenu = NULL;
	while(npmenu == NULL)
		npmenu = init_dialog(npdialog, -1);
	while(update_dialog(npmenu)) {
		while(update_count < 1) {
			rest(1);
		}
		update_count = 0;
		frame_count++;
	}
	shutdown_dialog(npmenu);
	
	destroy_bitmap(bgbmp);
	mute = oldmute;
}
