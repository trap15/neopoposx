#ifndef NEOPOP_CONF_H
#define NEOPOP_CONF_H

#include <neopop.h>

typedef struct {
	_u32	psgsamps;
	_u32	dacsamps;
	_u32	psgrate;
	_u32	callbacktime;
	_u8	scale;
} neopop_config_t;

extern neopop_config_t npconf;

void config_load();

#endif
