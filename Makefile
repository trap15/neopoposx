CC := gcc
LD := gcc
CFLAGS := -std=gnu99 -Wall -W -O2 -g `allegro-config --cflags` -ICore -ICore/TLCS-900h -ICore/z80 -m32
#CFLAGS += -DNEOPOP_DEBUG
LDFLAGS := `allegro-config --libs` -m32

# Is this compiler targetting MacOSX?
is-osx = $(shell set -e; \
        if ($(CC) -E -dM - | grep __APPLE__) </dev/null >/dev/null 2>&1; \
        then echo "yes" ; fi; )

OBJECTS := Core/bios.o Core/biosHLE.o Core/dma.o Core/flash.o Core/gfx_scanline_colour.o Core/gfx_scanline_mono.o Core/gfx.o Core/interrupt.o Core/mem.o Core/neopop.o Core/rom.o Core/sound.o Core/state.o Core/Z80_interface.o

OBJECTS += Core/TLCS-900h/TLCS900h_disassemble_dst.o Core/TLCS-900h/TLCS900h_disassemble_extra.o Core/TLCS-900h/TLCS900h_disassemble_reg.o Core/TLCS-900h/TLCS900h_disassemble_src.o Core/TLCS-900h/TLCS900h_disassemble.o Core/TLCS-900h/TLCS900h_interpret.o Core/TLCS-900h/TLCS900h_interpret_reg.o Core/TLCS-900h/TLCS900h_interpret_single.o Core/TLCS-900h/TLCS900h_interpret_src.o Core/TLCS-900h/TLCS900h_interpret_dst.o Core/TLCS-900h/TLCS900h_registers.o

OBJECTS += Core/z80/dasm.o  Core/z80/Z80.o

OBJECTS += Neopop_audio.o Neopop_comms.o Neopop_conf.o Neopop_io.o Neopop_msg.o Neopop_video.o Neopop_menu.o Neopop.o

ifeq ($(is-osx),yes)
OBJECTS += Neopop_cocoa.o
CFLAGS += -DUSING_OSX=1
endif

.PHONY: all
all: neopop

# The emulator.
neopop: $(OBJECTS)
	$(LD) $(LDFLAGS) -o $@ $(OBJECTS)

# If MacOSX, use Cocoa dialogs, and create an application bundle.
ifeq ($(is-osx),yes)

%.o: %.m
	$(CC) $(CFLAGS) -o $@ -c $<

all: .stamp-bundle

.stamp-bundle: neopop
	-mkdir -p NeoPop.app/Contents/Resources/
	-mkdir -p NeoPop.app/Contents/MacOS
	cp neopop NeoPop.app/Contents/MacOS/
	touch $@
endif

# Clean up.
.PHONY: clean
clean:
	rm -f $(OBJECTS) neopop 
ifeq ($(is-osx),yes)
	rm -f Neopop_cocoa.o NeoPop.app/Contents/MacOS/neopop
endif
