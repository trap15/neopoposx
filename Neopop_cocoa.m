// Copyright 2008-2010  Segher Boessenkool  <segher@kernel.crashing.org>
// Licensed under the terms of the GNU GPL, version 2
// http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt

#import <Cocoa/Cocoa.h>

void set_base_path(char* path)
{
	NSString *dir = [[NSBundle mainBundle] bundlePath];
	[dir getCString:path maxLength:256 encoding:NSASCIIStringEncoding];
	char *ptr;
	char *i;
	int len;
	ptr = strstr(path, ".app");
	if(ptr) {
		len = strlen(path);
		while(*ptr != '/') ptr--;
		for(i = ptr; i < (path + len); i++)
			*i = 0;
	}
	path[strlen(path)] = '/';
	path[strlen(path)] = 0;
}
