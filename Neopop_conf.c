#include <neopop.h>

#include "Neopop_conf.h"

neopop_config_t npconf;

void config_load()
{
	npconf.psgsamps		= 804 * 2;
	npconf.dacsamps		= 134 * 2;
	npconf.psgrate		= 48000;
	npconf.callbacktime	= 30;
	npconf.scale		= 2;
}
