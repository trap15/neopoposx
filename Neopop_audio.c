#include <allegro.h>
#include <neopop.h>

#include "Neopop_conf.h"

#define USE_STEREO 1
#define MAXSAMPLES		(65536)

#define PSGSAMPLES		(npconf.psgsamps)
#define DACSAMPLES		(npconf.dacsamps)

#define SOUND_CALLBACK_TIMING	(npconf.callbacktime)

static _u16 psgbuffer[MAXSAMPLES];
static _u16 psglbuffer[MAXSAMPLES];
static _u16 psgrbuffer[MAXSAMPLES];
static _u8  dacbuffer[MAXSAMPLES];

static AUDIOSTREAM* psgstream;
static AUDIOSTREAM* dacstream;

extern BOOL mute;

extern FILE* ngpmfile;

void system_sound_callback()
{
	_u16* buf;
	int i;
	if(psgstream) {
		buf = get_audio_stream_buffer(psgstream);
		if(buf != NULL) {
#if USE_STEREO
			sound_update_lr(psglbuffer, psgrbuffer, PSGSAMPLES << 1);
			if(!mute) {
				for(i = 0; i < PSGSAMPLES; i++) {
					buf[i * 2 + 0] = psglbuffer[i];
					buf[i * 2 + 1] = psgrbuffer[i];
				}
			}else{
				bzero(buf, PSGSAMPLES << 2);
			}
#else
			sound_update(psgbuffer, PSGSAMPLES << 1);
			if(!mute) {
				memcpy(buf, psgbuffer, PSGSAMPLES << 1);
			}else{
				bzero(buf, PSGSAMPLES << 2);
			}
#endif
			if(psgstream)
				free_audio_stream_buffer(psgstream);
		}
	}
	if(dacstream) {
		buf = get_audio_stream_buffer(dacstream);
		if(buf != NULL) {
			dac_update((_u8*)dacbuffer, DACSAMPLES);
			if(!mute)
				memcpy(buf, dacbuffer, DACSAMPLES);
			else
				bzero(buf, DACSAMPLES);
			if(dacstream)
				free_audio_stream_buffer(dacstream);
		}
	}
}

void system_sound_chipreset()
{
	printf("%s\n", __FUNCTION__);
	if(!psgstream)
		psgstream = play_audio_stream(PSGSAMPLES, 16, USE_STEREO, npconf.psgrate, 255, 128);
	if(!dacstream)
		dacstream = play_audio_stream(DACSAMPLES,  8,          0, DAC_FREQUENCY, 255, 128);
	sound_init(npconf.psgrate);
	install_int_ex(system_sound_callback, BPS_TO_TIMER(SOUND_CALLBACK_TIMING));
	return;
}

void system_sound_resume()
{
	if(!psgstream)
		psgstream = play_audio_stream(PSGSAMPLES, 16, USE_STEREO, npconf.psgrate, 255, 128);
	if(!dacstream)
		dacstream = play_audio_stream(DACSAMPLES,  8,          0, DAC_FREQUENCY, 255, 128);
	install_int_ex(system_sound_callback, BPS_TO_TIMER(SOUND_CALLBACK_TIMING));
}

void system_sound_silence()
{
	printf("%s\n", __FUNCTION__);
	stop_audio_stream(psgstream);
	stop_audio_stream(dacstream);
	psgstream = NULL;
	dacstream = NULL;
	return;
}

BOOL system_sound_init()
{
	printf("%s\n", __FUNCTION__);
	if(install_sound(DIGI_AUTODETECT, MIDI_NONE, NULL) != 0) {
		printf("Error initializing sound system: %s\n", allegro_error);
		return FALSE;
	}
	system_sound_chipreset();
	return TRUE;
}

void system_sound_update()
{
	return;
}

void system_sound_shutdown(void)
{
	printf("%s\n", __FUNCTION__);
	system_sound_silence();
	return;
}
